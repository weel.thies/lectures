---
search:
  exclude: true
---

```python tags=["initialize"]
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()
```

# Solutions for exercises lecture 4: Sommerfeld model

### Warm-up questions
1. See lecture notes
2. The free electron dispersion is

    $$
    \varepsilon(k) = \frac{\hbar^2 k^2}{2m},
    $$

    where $k=|\mathbf{k}|$.

3. The energy is
$$
E = \int \limits_0^{\infty} g(\varepsilon) n_{F}(\beta (\varepsilon - \mu)) \varepsilon \mathrm{d} \varepsilon
$$

4. The Fermi Surface is the collection of occupied states at the Fermi energy. In the free electron model, the Fermi surface is two points in 1D, a circle in 2D, and the surface of a sphere in 3D.
5. If the electrons in the solid can be described by the free electron model, the heat capacity is dominated by the electrons, since $T^3$ decreases faster than $T$ as $T\rightarrow 0$.

### Exercise 1*: Deriving the density of states for a parabolic dispersion relation.

1. The free electron dispersion is

    $$
    \varepsilon(k) = \frac{\hbar^2 k^2}{2m},
    $$

    where $k=|\mathbf{k}|$.

2. The distance between points in k-space is $ \Delta k = \frac{2 \pi}{L}$. The density of k-points is: $(\frac{L}{2\pi})^d$, where d = 1, 2, or 3 for 1D, 2D and 3D respectively.

3. The number of states is
    $$
    N_\text{states} = 2_s \left(\frac{L}{2\pi}\right)^d \int_{0<\varepsilon(\mathbf{k})< \varepsilon_0}\textrm{d} \mathbf{k},
    $$

    yielding:

    \begin{align}
    N_\text{states, 1D} & = 2_s \frac{L}{2\pi} \int_0^{k_0} 2 dk, \\
    N_\text{states, 2D} & = 2_s \left(\frac{L}{2\pi}\right)^2 \int_0^{k_0} 2\pi k dk, \\
    N_\text{states, 3D} & = 2_s  \left(\frac{L}{2\pi}\right)^3 \int_0^{k_0} 4\pi k^2 dk.
    \end{align}

4. To transform the integral from one over k-space to one over energy, we need the dispersion relation. The integral boundaries are $0<\varepsilon<\varepsilon_0$. Using $k=|\mathbf{k}| = \sqrt{2m\varepsilon/\hbar^2}$ and $dk = \sqrt{m/(2\varepsilon\hbar^2)}d\varepsilon $, we get:

    \begin{align}
    g_{1D}(\varepsilon) &= 2_s \frac{L}{2\pi} \sqrt{\frac{2m}{\hbar^2\varepsilon}},\\
    g_{2D}(\varepsilon) &= 2_s \frac{L^2}{2\pi}\frac{m}{\hbar^2},\\
    g_{3D}(\varepsilon) &= 2_s \frac{L^3}{2\pi^2}\frac{m}{\hbar^2}\sqrt{\frac{2m\varepsilon}{\hbar^2}} .
    \end{align}

### Exercise 2: Applying the free electron model to potassium

1. Alkali metals mostly have a spherical Fermi surface, just like the electrons in the free electron model.
2. Refer to the lecture notes for the plot. $k_F = 7.5 \cdot 10^9 \text{m}^{-1}$, $v_F = 8.6 \cdot 10^5 m/s$ and $T_F = 24600 K$.

3. The free-electron density is:

    $$
    n = \frac{N}{V} = \frac{1}{3 \pi^{2} \hbar^{3}}\left(2 m \varepsilon_{F}\right)^{3 / 2} = 1.4 \cdot 10^{28} \, \text{m}^{-3}
    $$

4. The total electron density is

    $$
    n_t = \frac{\rho N_A Z}{M} = 2.6 \cdot 10^{29} \, \text{m}^{-3},
    $$

where $\rho$ is the mass density, $N_A$ is the Avogadro's constant, $M$ is molar mass and $Z$ is the valence of potassium atom.
We see that the free-electron density is much lower than the total electron density. We conclude that only a small fraction of the electrons are available for conduction -  roughly 1 free electron per potassium atom.

### Exercise 3: The electron dispersion, density of states, and heat capacity of graphene

1. The electron dispersion is plotted here as a function of $k_x$ for $k_y=0$.

```python
import numpy as np
import matplotlib.pyplot as plt
x = np.linspace(-1, 1, 100)
fig, ax = plt.subplots(figsize=(7, 5))
ax.plot(x, x, 'b')
ax.plot(x,-x, 'b')

ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position(('data', 0.0))

# Eliminate upper and right axes
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')

ax.set_xticks([])
ax.set_yticks([])

ax.set_xlabel(r'$k_x$', fontsize=14)
ax.set_ylabel(r'$\varepsilon$', fontsize=18, rotation='horizontal')
ax.yaxis.set_label_coords(0.5,1)
ax.xaxis.set_label_coords(1.0, 0.49)
```

2\. We recall from the Debye lecture that the DOS of a 2D linear dispersion is linear in energy. A similar calculation for graphene shows that the DOS, for positive energies is given by

$$
g(\varepsilon) = 2_s 2_v 2 \pi \left(\frac{L}{2 \pi}\right)^2 \frac{\varepsilon}{c^2},
$$

where $2_s$ is the spin degeneracy and $2_v$ is the valley degeneracy.
From the symmetry of the dispersion, it immediately follows that if we account for the negative energies as well, we obtain

$$
g(\varepsilon) = 2_s 2_v 2 \pi \left(\frac{L}{2 \pi}\right)^2 \frac{|\varepsilon|}{c^2}.
$$

3\. Since we have $g(\varepsilon)\propto \varepsilon$ and $E_F=0$, the number of electrons that get thermally excited is given by the area of a triangle with base $k_BT$ and height $g(-k_BT)$:

\begin{align}
n_{ex} &= \frac{1}{2} g(-k_B T) k_B T\\
&= \frac{L^2 k_B^2T^2}{\pi c^2}.
\end{align}

From this it follows that the energy difference is given by

$$
E(T) - E_0 = \frac{L^2 k_B^3T^3}{\pi c^2}.
$$

4\. The heat capacity is
    $$
    C_v(T) = \frac{\partial E}{\partial T} = \frac{3L^2k_B^3T^2}{\pi c^2}
    $$

### Exercise 4: Two energy bands

```python
fig, ax = pyplot.subplots()
x = np.linspace(-3, 3, 100)
ax.plot(x, x**2, 'b')
ax.plot(x, x**2 + 1, 'b')
ax.axhline(y=2, color='r', linestyle='-')
ax.set_xticks([])
ax.set_yticks([0,1,2]);
ax.set_yticklabels([r'$0$',r'$\varepsilon_0$',r"$E_F$"])
ax.set_ylabel(r"$\varepsilon$")
ax.set_xlabel(r"$k$")
ax.set_ylim(-.1, 4)
draw_classic_axes(ax, xlabeloffset=.2)
```

1. We have sketched the two bands in the plot above. The Fermi energy is indicated by the red line.

2. We compute the density of states separately for each band and add these partial density of states to obtain the total density of states. Note that for the first band there will be no states below $\varepsilon <0$ and for the second band no states below $\varepsilon<\varepsilon_0$.

    Therefore, we have $g(\varepsilon)=A$ for $0<\varepsilon<\varepsilon_0$ and $g(\varepsilon)=2A$ for $\varepsilon>\varepsilon_0$, where $A=2_s \frac{L^2}{2\pi}\frac{m}{\hbar^2}$ is the constant DOS we found for a 2D parabolic dispersion in exercise 1.4.

3. At $T = 0$, the Fermi-Dirac distribution is a step function. The number of electrons is

    $$
    N = \int_0^{E_F}g(\varepsilon)d\varepsilon = A\varepsilon_0 + 2A(E_F-\varepsilon_0)
    $$

4. The total number of electrons between $\varepsilon_a<\varepsilon<\infty$ is

    $$
    N(\varepsilon_a<\varepsilon<\infty) = \int_{\varepsilon_a}^\infty g(\varepsilon) \frac{1}{e^{\beta (\varepsilon-E_F)}+1} \textrm{d}\varepsilon
    $$

5. Because $\varepsilon_a-E_F \gg k_B T$, the Fermi-Dirac can be approximated by the Boltzmann distribution: $n_F \approx e^{-\beta (\varepsilon-E_F)}$. Working out the integral we obtain

    $$
    N(\varepsilon_a<\varepsilon<\infty)  = \int_{\varepsilon_a}^\infty 2A  e^{-\beta (\varepsilon-E_F)} \textrm{d}\varepsilon = 2A k_B T e^{-\beta (\varepsilon_a-E_F)}
    $$
