---
search:
  exclude: true
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.0'
      jupytext_version: 0.8.6
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python tags=["initialize"]
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import quad

from common import draw_classic_axes, configure_plotting

configure_plotting()
```

# Solutions for lecture 1 exercises
### Warm-up exercises
1. By definition, the particles in an ideal gas do not interact. As such, their energy is independent of their position, and only depends on the momenta of the particles. This means that only the 3 momentum degrees of freedom can store energy and thereby contribute to the heat capacity. In contrast, both the position and the momentum of atoms described by harmonic oscillators (e.g. masses on springs) determine the energy. This means that  3 momentum + 3 positional degrees of freedom can store energy and thereby contribute to the heat capacity. Finally, equipartition tells us that each degree of freedom contributes $1/2k_BT$ to the energy.
2. Following last question's reasoning, we get $C = 2k_B$ per atom.
3. See the plot with the slider in the lecture notes. For fun, the Einstein dist. for two different values of $T$ is plotted below

```python
fig, ax = plt.subplots()
omega = np.linspace(0.1, 3)
T = [1,2]
ax.plot(omega, 1/(np.exp(omega/T[0]) - 1), label = r'$T_1$')
ax.plot(omega, 1/(np.exp(omega/T[1]) - 1), label = r'$T_2$')
ax.set_ylim([0,3])
ax.set_xlim([0,3])
ax.set_xlabel('$\hbar \omega$')
ax.set_xticks([0])
ax.set_xticklabels(['$0$'])
ax.set_ylabel('$n_B$')
ax.set_yticks([0,1, 2])
ax.set_yticklabels(['$0$','$1$', '$2$'])
ax.legend()
draw_classic_axes(ax, xlabeloffset=.2)
```
### Exercise 1: The harmonic oscillator and the Bose-Einstein distribution¶

### Exercise 2: The quantum harmonic oscillator - connection with statistical physics

1. The energy spectrum is $E_n = (n+1/2)\hbar\omega_0$ with $n=0,1,2,...$
2. We calculate the partition function using 
    $$ Z = \sum_{n = 0}^{\infty} e^{-\beta\hbar\omega(n + 1/2)} = e^{-\beta\hbar\omega/2}  \frac{1}{1 - e^{-\beta\hbar\omega}} = \frac{1}{2\sinh(\beta\hbar\omega/2)} $$

    ,where we used the result of a geometric series $\sum_{n = 0}^{\infty}r^n = \frac{1}{1 - r}$.

3. We calculate the expectation value of the energy using $E = d\ln(Z)/d\beta)$. This yields
    $$ E = -\frac{1}{Z}\frac{\partial Z}{\partial\beta} = \frac{\hbar\omega}{2}\coth\frac{\beta\hbar\omega}{2} = \hbar\omega\left(\frac{1}{e^{\beta\hbar\omega} - 1} + \frac{1}{2}\right) = \hbar\omega\left(n_B(\beta\hbar\omega) + \frac{1}{2}\right). $$

4. We use $C = dE/dT$, yielding 
    $$ C = \frac{\partial \langle E\rangle}{\partial T} = \frac{\partial\langle E\rangle}{\partial\beta}\frac{\partial\beta}{\partial T} = k_B(\beta\hbar\omega)^2\frac{e^{\beta\hbar\omega}}{(e^{\beta\hbar\omega} - 1)^2}. $$

    In the high temperature limit $\beta \rightarrow 0$ and $e^{\beta\hbar\omega} \approx 1 + \beta\hbar\omega$, so $C \rightarrow k_B$ which is the same result as in Exercise 1.3.

5. It is given by the Bose-Einstein distribution. This also follows from the partition function using 
    $$ \langle n\rangle = \frac{1}{Z}\sum_{n = 0}^{\infty} ne^{-\beta\hbar\omega(n + 1/2) } = 2\frac{e^{\beta\hbar\omega/2} - e^{-\beta\hbar\omega/2}}{2}e^{-\beta\hbar\omega/2}\frac{e^{-\beta\hbar\omega}}{(1 - e^{-\beta\hbar\omega})^2} = \frac{1}{e^{\beta\hbar\omega} - 1} $$

  where we used $\sum_{n = 0}^{\infty}nr^n = \frac{r}{(1 - r)^2}$.

### Exercise 3*: Total heat capacity of a diatomic material.

1. We use $\omega_{1,2} = \sqrt{\frac{k}{m_{1,2}}}$.

2. The expectation value of the vibrational energy stored in a single atom modeled as a 3D harmonic oscillator with eigenfrequency $\omega_{1,2}$  is given by 
    $$ E_{1,2} = 3 (n(\omega_{1,2})+1/2)\hbar\omega_{1,2} $$ , 
    where $n(\omega)$ is given by the Bose-Einstein distribution function. The total energy $E$ of the crystal is obtained by summing over all the atoms: $E = N_1E_1 + N_2E_2$, where $N_1$ is the number of $^6$Li atoms and $N_2$ is the number of $^7$Li  atoms.
3. The heat capacity is given by $dE/dT$, with $E$ given in the answer to the previous subquestion. The calculation is the same as that in the lecture notes (apart from the factor 3 for the dimensionality). 

A harmonic oscillator starts to go mostly into its ground state when the temperature is reduced below its Einstein temperature (Note that the Einstein temperature is the eigenfrequency of the oscillator expressed in units of Kelvin). Therefore, upon reducing temperature, first the atoms with the higher eigenfrequency (or equivalently, higher Einstein temperature) will go into their ground state, and then the atoms with the lower eigenfrequency. 

