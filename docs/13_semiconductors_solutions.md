---
search:
  exclude: true
---
```python tags=["initialize"]
from matplotlib import pyplot as plt 
import numpy as np
from math import pi
```
# Solutions for lecture 13 exercises

## Exercise 1: Energy, mass, velocity and cyclotron motion of electrons and holes

#### Question 1.

Electrons near the top of the valence band have a negative effective mass, their energy decreases as $k$ increases from 0, and they have a negative group velocity for $k>0$.

#### Question 2.

Holes near the top of the valence band have a positive effective mass, their energy increases as $k$ increases from 0, and they have a negative group velocity for $k>0$.

#### Question 3.

The equation of motion for an electron near the bottom of the conduction band is:

$$
m^* \frac{d\mathbf{v}}{dt} = -e\mathbf{v} \times \mathbf{B}
$$

and when replacing we get two coupled equations:

\begin{align}
\dot{k_x} &= -\frac{e}{m^*}B k_y\\
\dot{k_y} &= +\frac{e}{m^*}B k_x
\end{align}

The solution to this equation is circular motion of cyclotron frequency of $\omega_c = \frac{eB}{m^*}$, where the Lorentz
force is perpendicular to $\nabla_\mathbf{k} E$.

#### Question 4.

A hole near the bottom of the conduction band will have the same chirality as an electron.
The chirality would be just the opposite if we would consider the valence band (for both electrons and holes).



### Exercise 2: The Hall coefficient when both electrons and holes are present in the system

#### Question 1.

$$
R_{H,h} = \frac{1}{ne}
$$

#### Question 2.

Filling in $n_e = n_h$ and $\mu_e=\mu_h$ results in $R_H = 0$.

### Exercise 3. Holes and electrons in a 1D tight-binding energy band

#### Question 1.

For electrons we find

$$
m_e = -\frac{\hbar^2}{2ta^2\cos(ka)}
$$

$$
v_e = -\frac{2ta\sin(ka)}{\hbar},
$$

and for holes we obtained

$$
m_h = -m_e
$$

$$
v_h = v_e.
$$

Thus the effective masses of electrons an hole will be of opposite sign, while the group velocities will be the same!

#### Question 2.

The number of holes is defined as

$$ 
N_h = \int_{-\varepsilon-2t}^{-\varepsilon+2t} f(E_h, E_{F,h}) g_h(E_h)d E_h,
$$

where $g_h(E_h)$ is the density of states of the holes in terms of hole energy $E_h$.

#### Question 3.

??? hint "Small hint"

    It is convenient to write the hole integral in terms of the electron energy. 
    


## Exercise 4: a 1D semiconductor

```python
def dispersion(EG, tcb, tvb, N=100, kmax=np.pi/2):
    a = 1
    kx = np.linspace(-kmax, kmax, N)
    Ecb = EG - 2*tcb*(np.cos(kx*a)-1)
    Evb = 2*tvb*(np.cos(kx*a)-1)
    
    # Plot dispersion
    plt.figure(figsize=(6,5))
    cb, = plt.plot(kx, Ecb, label="Conduction B.")
    vb, = plt.plot(kx, Evb, label="Valence B.")

    plt.xlabel('$k_x$', fontsize=20)
    plt.ylabel('$E$', fontsize=20)
    plt.title('E(k) for tcb:'+str(tcb)+' tvb:'+str(tvb))
    plt.legend(handles=[cb, vb])

dispersion(10, 2, 8)
```

#### Question 1.

For the electrons in the conduction band we find

$$
v_{cb,e} = \frac{2at_{cb}}{\hbar}\sin (ka)
$$

$$
m_{cb,e} =  \frac{\hbar^2}{2a^2t_{cb}\cos (ka)}.
$$

For the holes in the valence band we obtain

$$
v_{vb,h} = -\frac{2at_{vb}}{\hbar}\sin (ka)
$$

$$
m_{vb,h} =  -\frac{\hbar^2}{2a^2t_{vb}\cos (ka)}.
$$

#### Question 2.

This approximation indicates the chemical potential is "well below" the conduction band and "well above"
the valence band. 
This way, only a few electrons occupy the the states near the bottom of the conduction band and only a few holes occupy the states near the top of the valence band.
This allows us the approximate the bottom of the conduction band and the top of the valence band as parabolic dispersions.
In addition, the Fermi-Dirac distribution can be approximated as the Boltzman distribution.

The dispersions of the valence and conduction band are approximately equal to

$$
E_{cb} \approx E_G + t_{cb}(ka)^2
$$

$$
E_{vb} \approx -t_{vb}(ka)^2
$$

#### Question 3.

For the density of states for both the electrons and holes we find

$$
g_e(E) = \frac{1}{\pi a \sqrt{t_{cb}(E-E_G)}}, \:\: \text{if} \:\: E > E_G,
$$

and 

$$
g_h(E_h) = \frac{1}{\pi a \sqrt{t_{vb}E_h}}, \:\: \text{if} \:\: E_h > 0,
$$

where $E_h = -E$.

#### Question 4.

The electron density in the conduction band is given by

\begin{align}
n_e &= \int_{E_G}^{\infty} f(E)g_c(E)dE\\
&\approx \int_{E_G}^{\infty} e^{-\beta (E-\mu)} g_c(E) dE\\
&=\sqrt{\frac{k_B T}{\pi t_{cb}}}\frac{e^{\beta (\mu - E_G)}}{a}.
\end{align}

Analoguous, we find for the hole density

\begin{align}
n_h &= \int_{0}^{\infty} f(E_h) g_h(E_h) dE_h\\
&\approx \sqrt{\frac{k_B T}{\pi t_{vb}}}\frac{e^{-\beta \mu}}{a}.
\end{align}

#### Question 5.

In the intrinsic regime $n_e = n_h$.
Solving this for $\mu$ results in

$$
\mu = \frac{1}{2}E_G + \frac{k_B T}{4}ln(\frac{t_{cb}}{t_{vb}})
$$

