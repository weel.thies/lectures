```python tags=["initialize"]
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()

pi = np.pi
```

# Atoms and bonds
_(based on chapters 5 and 6.2 of the book)_

!!! success "Expected prior knowledge"

    Before the start of this lecture, you should be able to:

    - Write down the Schrödinger equation
    - Solve the Schrödinger equation of a particle in a 1D $δ$-function potential well (for the exercises)
    - Compute the eigenvectors and eigenvalues of a matrix
    - Write down the quantum numbers of the hydrogen atom
    - Describe the orbitals of the hydrogen atom and their energies using the quantum numbers

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Describe the shell-filling model of atoms
    - Describe the linear combination of atomic orbitals (LCAO) model
    - Define and calculate 'onsite' and 'hopping' energies
    - Construct the LCAO Hamiltonian and solve it to obtain the energy spectrum of small molecules

## Looking back

So far we have:

* Introduced the $k$-space (reciprocal space)
* Postulated the dispersion relation of free electrons and phonons
* Calculated the heat capacity of free electrons and phonons

As a result we:

* Understood how phonon modes store heat (Debye model)
* Understood how free electrons conduct (Drude model) and store heat/energy (Sommerfeld model)

We made several approximations and postulations through these models.
However, there are still several mysteries:

* Why is there a phonon cutoff frequency? Why are there no more phonon modes beyond this cutoff frequency?
* Why don't electrons scatter off every single atom in the Drude model?
Atoms are charged and should provide a lot of scattering.
* Why are some materials not metals? (Think if you know a crystal that isn't a metal)

To answer these questions we will need to study atoms in more detail.

## A quick review of the electronic orbitals of atoms

Our goal is to understand the energy spectrum of electrons in solids. To do so, we will start with studying the basic building blocks: the atoms.
In principle, we know how to calculate the energies of the electrons in solids because **everything** is described by the Schrödinger equation:

$$
\hat{H}\psi = E\psi,
$$

where $H$ is the Hamiltonian, which is the sum of the kinetic and potential energy operators.
In the hydrogen atom, the potential energy is due to the Coulomb interaction between the electron and the nucleus:

$$
\hat{H}=-\frac{\hbar^2}{2m}\frac{\partial^2}{\partial {\mathbf r^2}} - \frac{e^2}{4\pi\varepsilon_0|r|}.
$$

In the case of helium, the Hamiltonian already becomes much more complex: it contains not just the Coulomb attraction between the electrons and the nuclei, but also the Coulomb repulsion between the two electrons:

$$
\hat{H}=-\frac{\hbar^2}{2m}\frac{\partial^2}{\partial {\mathbf r_1^2}} -\frac{\hbar^2}{2m}\frac{\partial^2}{\partial {\mathbf r_2^2}}- \frac{2e^2}{4\pi\varepsilon_0|r_1|} - \frac{2e^2}{4\pi\varepsilon_0|r_2|} + \frac{e^2}{4\pi\varepsilon_0|r_1 - r_2|},
$$

which means we need to find eigenvalues and eigenvectors of a 6-dimensional partial differential equation, instead of the 3-dimensional one describing hydrogen.

Even "every-day" copper has 29 electrons, so to find the electron spectrum of copper we would need to solve an 87-dimensional Schrödinger equation.
There is no way in the world we can do so even with the most powerful modern computers!

The growth in complexity with the number of interacting quantum particles is why *many-body quantum physics* is very much an open area in solid state physics and quantum chemistry.

We need to focus on what is possible to do, and apply heuristic rules based on the accumulated knowledge of how atoms work (hence we will need a bit of chemistry).

The complexity of solving the problem is the reason why we need to accept empirical observations as *chemical laws* even though they work with limited precision, and are actually consequences of the Schrödinger equation.

### Quantum numbers and shell filling

The electronic orbitals of a hydrogen atom can be described by the following four quantum numbers: $|n, l, l_z, m_s\rangle$.

Quantum numbers:

* $n=1,2,\ldots$ is the azimuthal (principal) quantum number. It determines the energy of the orbital
* $l=0, 1, \ldots, n-1$ is the angular momentum (also known as $s, p, d, f$ orbitals)
* $l_z=-l, -l+1\ldots, l$ is the $z$-component of angular momentum
* $m_s$ is the $z$-compoment of the spin

Below is an illustration of a few of the lowest-energy orbitals in hydrogen:

[![Hydrogen atomic orbitals](https://upload.wikimedia.org/wikipedia/commons/5/5c/Atomic_orbitals_n123_m-eigenstates.png)](https://commons.wikimedia.org/wiki/File:Atomic_orbitals_n123_m-eigenstates.png#/media/File:Atomic_orbitals_n123_m-eigenstates.png){: style="width:50%"}

(image source: Wikipedia © Geek3 CC-BY-SA)

It turns out that electrons in other atoms occupy orbitals similar to those of hydrogen, although the electron energies are very different due to the Coulomb interaction.
Because of this similarity, we can use our knowledge of the hydrogen orbitals to describe other atoms.

When we consider an atom with multiple electrons, we need to determine which orbitals are filled and which are not.
We can predict the order of orbital filling using several rules:

* _Aufbau principle_: electrons first fill a complete shell (all electrons with the same $n$) before going to the next one
* _Madelung's rule_: electrons first occupy the shells with the lowest $n+l$. If there are several orbitals with equal $n+l$, electrons occupy those with smaller $n$

Combining the two rules, we obtain the shell filling order: 1s, 2s, 2p, 3s, 3p, 4s, 3d, etc.

While these rules accurately predict the electronic structure of most elements, they are only approximate, and fail to describe some of the [heavier elements](https://en.wikipedia.org/wiki/Aufbau_principle#Exceptions_in_the_d-block).

Shell filling is important to us because the valence electrons (those in the outermost shell) are the only ones participating in chemical reactions and electric conduction.
From the valence electrons' point of view, the inner shell electrons act like a negatively charged cloud.
The electrostatic repulsion between them reduces the effective charge of the atomic nucleus, but does not play any further role.

## Covalent bonds and linear combination of atomic orbitals (LCAO)

### A two-atom molecule
Consider two atoms next to each other, forming a diatomic molecule.
The Hamiltonian describing the energy of an electron on the molecule is

$$
\hat{H} = \hat{V}_1 + \hat{V}_2 + \hat{K},
$$

with $\hat{V}_1$ the potential energy due to the first nucleus, $\hat{V}_2$ due to the second nucleus, and $\hat{K}$ the kinetic energy of the electron.

Since the different orbitals of an atom are separated in energy, we consider only one orbital per atom (even though this is often a bad starting point and it should only work for $s$-orbitals).

Additionally, we assume that the atoms are sufficiently far apart, such that the shape of the orbitals barely changes due to the presence of the other atom.

If the atoms are far apart from each other such that they do not interact, the eigenstates of the electrons are the atomic orbitals. If we call the atomic orbital of the electron on the first atom $|1\rangle$ and that of the electron on the second atom $|2\rangle$, we have:

\begin{align}
(\hat{V}_1 + \hat{K})|1\rangle = \varepsilon_0|1\rangle,\\
(\hat{V}_2 + \hat{K})|2\rangle = \varepsilon_0|2\rangle.
\end{align}

Key idea: to find the wavefunction of the electron on the molecule - the _molecular orbital_ -, we search for a solution that is a *linear combination of the atomic orbitals* (LCAO):

$$
|\psi\rangle = \phi_{1}|1\rangle + \phi_{2}|2\rangle.
$$

where $\phi_{1}$ and $\phi_{2}$ are probability amplitudes. The orbital $|\psi\rangle$ is called a molecular orbital because it describes the eigenstate of an electron on the diatomic molecule.

For simplicity, we assume that the atomic orbitals are orthogonal[^1], i.e. $\langle1|2\rangle=0$. This orthogonality ensures that $|\psi\rangle$ is normalized whenever $|\phi_1|^2 + |\phi_2|^2 = 1$.

To find the possible values of $\phi_{1}$ and $\phi_{2}$ and the associated eigenenergies of the molecular orbitals, we apply the full Hamiltonian to $|\psi\rangle$:

$$
H|\psi\rangle = E|\psi\rangle = \phi_{1}H|1\rangle + \phi_{2}H|2\rangle.
$$

Taking the left inner product with $\langle1|$, we obtain

$$
\langle1|E|\psi\rangle = \phi_{1}\langle1|\hat{H}|1\rangle + \phi_{2}\langle1|\hat{H}|2\rangle = E \phi_{1}.
$$

Similarly, taking the inner product with $\langle2|$ yields:

$$
E \phi_2 = \phi_{1}\langle2|\hat{H}|1\rangle + \phi_{2}\langle2|\hat{H}|2\rangle.
$$

We combine these two equations into an eigenvalue problem:

$$
E \begin{pmatrix} \phi_1 \\ \phi_2 \end{pmatrix}
= \begin{pmatrix}
\langle1|\hat{H}|1\rangle & \langle1|\hat{H}|2\rangle \\ \langle2|\hat{H}|1\rangle & \langle2|\hat{H}|2\rangle
\end{pmatrix}
\begin{pmatrix} \phi_1 \\ \phi_2\end{pmatrix}.
$$

The eigenvalue problem depends on only two parameters: the **onsite energy** $\langle1|\hat{H}|1\rangle = \langle2|\hat{H}|2\rangle \equiv E_0$ that gives the energy of an electron occupying either of the atomic orbitals, and the **hopping integral** (or just **hopping**) $\langle1|\hat{H}|2\rangle \equiv -t$ that characterizes the energy associated with the electron moving between the two orbitals.

First, let us examine what constitutes the onsite energy and the hopping:

$$
E_0 = \langle1|\hat{H}|1\rangle = \langle1|\hat{V}_1 + \hat{V}_2 + \hat{K}|1\rangle = \varepsilon_0 + \langle1|\hat{V}_2|1\rangle,
$$

where we used that $(\hat{V}_1 + \hat{K})|1\rangle = \varepsilon_0|1\rangle$.
In other words, the onsite energy is the combination of the energy of the original orbital plus the energy shift $\langle1|\hat{V}_2|1\rangle$ of the electron due to the potential of the neighboring atom.
Second, the hopping is given by:

$$
t = -\langle1|\hat{H}|2\rangle = -\langle1|\hat{V}_1 + \hat{V}_2 + \hat{K}|2\rangle = -\langle1|\hat{V}_1|2\rangle.
$$

The orbitals $|n\rangle$ are purely real because we consider bound state solutions of the Schrödinger equation.
Hence $t$ is real as well.

The eigenvalue problem we obtained describes a particle with a discrete $2×2$ Hamiltonian:

$$
H = \begin{pmatrix} E_0 & -t \\ -t & E_0 \end{pmatrix}.
$$

Diagonalizing this LCAO Hamiltonian yields the following two eigenvalues:

$$
E_{\pm} = E_0 \mp t.
$$

The eigenvector corresponding to the eigenvalue $E_+ = E_0 - t$ is even and symmetric:

$$
|\psi_{+}\rangle = \tfrac{1}{\sqrt{2}}(|1\rangle + |2\rangle),
$$

while the eigenvector with energy $E_- = E_0 + t$

$$
|\psi_{-}\rangle = \tfrac{1}{\sqrt{2}}(|1\rangle - |2\rangle)
$$

is odd/antisymmetric.

The molecular orbitals are shown in the figure below.
According to the *node theorem* of quantum mechanics, wave functions with lower energies have fewer points where $\psi=0$.
Because $\psi_- = 0$ between the two atoms, and $\psi_+$ is not, we conclude that $E_+ < E_-$, and therefore $t > 0$.

```python
fig, ax = pyplot.subplots()
x = np.linspace(-1, 4, 100)
r_1, r_2 = 0.5, 2.51
U = -1/np.abs(x - r_1) - 1/np.abs(x - r_2)
psi_1 = 0.3 * np.exp(-2*(x - r_1)**2)
psi_2 = 0.3 * np.exp(-2*(x - r_2)**2)
pyplot.fill_between(x, psi_1 + psi_2 - .5, -.5, alpha=.5, label="$\psi_+$")
pyplot.fill_between(x, psi_1 - psi_2 + .5, .5, alpha=.5, label="$\psi_-$")
pyplot.plot(x, U, label="$U(x)$", c='k', linestyle=':')
pyplot.xlabel("$x$")
pyplot.ylabel("$\psi$")
pyplot.legend(loc=(0.8, 0.3))
pyplot.ylim(-3, 1)
draw_classic_axes(ax)
```

### Bonding and antibonding orbitals
If we decrease the interatomic distance, the two atoms get closer and their atomic orbitals start to overlap more.
This increase in orbital overlap increases the hopping $t$.
We plot the symmetric and anti-symmetric energies as a function of the inter-atomic distance:

```python
fig, ax = pyplot.subplots()
r = np.linspace(0.2, 3, 100)
U = np.exp(-r)
ax.plot(r, + U, label="$E_-$")
ax.plot(r, - U, label="$E_+$")
ax.set_xlabel("$r$")
ax.set_ylabel("$E$")
ax.set_xlim(-.1, 3.2)
ax.legend()
draw_classic_axes(ax)
```

When an electron (or two, because there are two states with opposite spin) occupies $|\psi_+\rangle$, the atoms attract (or *bond*) because the total energy is lowered.
Therefore, if $t$ is positive, $|\psi_+\rangle$ is called the **bonding orbital**.

If an electron occupies the $|\psi_{-}\rangle$ orbital, the molecular energy increases with decreasing interatomic distance.
This means that the atoms repel each other.
Hence, if $t$ is positive, $|\psi_{-}\rangle$ is called the **antibonding orbital**.

Therefore if each atom has a single electron in the outermost shell, these atoms attract because the bonding orbital hosts two electrons with opposite spins.
On the other hand, if each atom has 0 or 2 electrons in the outermost shell, the net force from the bonding and antibonding orbitals cancels out, but Coulomb repulsion remains.

### Summary

* Electrons in atoms occupy shells, with only electrons in the outermost shell (valence electrons) contributing to interatomic interactions.
* The molecular orbital can be written as a Linear Combination of Atomic Orbitals (LCAO)
* The LCAO method reduces the full Hamiltonian to a finite-size problem written in the basis of the individual atomic orbitals.
* If two atoms have one orbital and one electron each, the two electrons occupy the bonding orbital.

## Exercises
### Warm-up questions*
1. Is the assumption that the atomic orbitals are orthogonal always a reasonable assumption?
2. What is the effect on the molecular orbitals if the hopping $t$ is chosen to be negative?
3. How does the size of the Hamiltonian matrix change with the number of atoms?
4. How does the size of the Hamiltonian matrix change if each atom now has two orbitals?
5. Write down the matrix form of the LCAO Hamiltonian for 3 atoms that each have one orbital, assuming and onsite energy $E_0$ for all 3 atoms, and a hopping $-t$ between atoms 1 and 2 and between atoms 2 and 3. (No calculations!). How would the Hamiltonian change if we also include a hopping $-t$ between atoms 1 and 3?
6. Assuming that we have two atoms with a single orbital each, what is the size of the Hamiltonian matrix if we also consider the spin of the electron?

### Exercise 1*: Shell-filling model of atoms

Here we consider the order in which electrons fill the orbitals on the atoms

1. Describe the shell-filling model of atoms.
2. Use Madelung’s rule to deduce the atomic shellfilling configuration of the element tungsten, which has atomic number 74.
3. Although Madelung’s rule for the filling of electronic shells holds extremely well, there are a number of exceptions to the rule. Here are a few of them:
$\textrm{Cu} = [\textrm{Ar}] 4s^1 3d^{10}$, $\textrm{Pd} = [\textrm{Kr}] 5s^0 4d^{10}$, $\textrm{Ag} = [\textrm{Kr}] 5s^1 4d^{10}$, $\textrm{Au} = [\textrm{Xe}] 6s^1 4f^{14} 5d^{10}$.
What should the electron configurations be if these elements followed Madelung’s rule and the Aufbau principle?
What could be the reason for the deficiency of Madelung's rule?

### Exercise 2*: Eigenstates of a 3-atom molecule
In this exercise, we calculate the eigenstates of a 3-atom molecule. By making use of the symmetry to guess the form of the eigenvectors, the problem becomes much easier to solve. We also analyze a limiting case in which the system starts to resemble two simpler systems, which enables us to quickly understand and write down the eigenstates.

1. Consider a molecule with 3 atoms with one orbital each. Each atom has onsite energy $E_0$, and there is a hopping $-t$ between nearest-neighbour atoms. Write down the LCAO Hamiltonian matrix for this system (no calculations). (Same as warm-up question 5.)

2. Discuss the interpretation of an eigenvector of this matrix: What do its components represent?

3. Consider the symmetry of the molecule to guess the form of the eigenvectors. Write the eigenvectors in terms of only one unknown parameter, and call this parameter $a$.

    ??? hint "Considering the symmetry"

        Diagonalizing matrices larger than 2x2 is generally a lengthy procedure. However, if we can guess the form of the eigenvectors, it becomes easy to check if they are indeed eigenvectors and to calculate the corresponding eigenvalues. To do so for this molecule, consider an eigenfunction $|\psi \rangle = \phi_1|1\rangle + \phi_2|2\rangle + \phi_3|3\rangle$. Considering the symmetry of the molecule and its Hamiltonian, do you expect the probabilities of finding the electron on atom 1 or on atom 3 to be different? Use this to write down the expected eigenvectors in a form that contains only one unknown parameter, which we will call $a$.

4. Apply the Hamiltonian to these eigenvectors to determine the possible values of $a$, and extract the eigenenergies.

5. Suppose we let the hopping between atoms 2 and 3 go to zero. What do you expect for the eigenenergies of the system? And for the eigenvectors?

    ??? hint "Identifying similarities with simpler systems"

        In the limiting case that the hopping between atoms 2 and 3 goes to zero, the system starts to resemble two independent, simpler systems. Which ones? Do you know the eigenenergies of these simpler systems?

### Exercise 3: Explicit calculation of the onsite and hopping energies from the wave functions of the atomic orbitals

In the LCAO model, we construct the molecular orbitals from linear combinations of the atomic orbitals. In this exercise, we model the atomic orbital of each individual atom as the bound state of a delta-function potential. To construct the LCAO Hamiltonian, we explicitly calculate the onsite energy and the hopping from these atomic orbitals. Diagonalizing this Hamiltonian yields the molecular orbitals. 

Consider an electron moving in 1D between two negative delta-function potential wells.
The full Hamiltonian of this one-dimensional system is:

$$
\hat{H} = \frac{\hat{p}^2}{2m}-V_0\delta(x_1-x)-V_0\delta(x_2-x),
$$

where $V_0>0$ is the potential strength, $\hat{p}$ the momentum of the electron, and $x_1$ and $x_2$ are the positions of the potential wells.

??? hint "Properties of a single $\delta$-function potential"

    A delta function $\delta(x_0 - x)$ centered at $x_0$ is defined to be zero everywhere except for $x_0$, where it tends to infinity.
    Further, a delta function has the property:

    $$
    \int_{-\infty}^{\infty} f(x)\delta(x_0-x)dx = f(x_0).
    $$

    The procedure to find the energy and a wave function of a state bound in a $\delta$-function potential, $V=-V_0\delta(x-x_0)$, is similar to that of a quantum well:

    1. Assume that we have a bound state with energy $E<0$.
    2. Compute the wave function $\phi$ in different regions of space: namely $x < x_0$ and $x > x_0$.
    3. Apply the boundary conditions at $x = x_0$. The wave function $\phi$ must be continuous, but this is not the case for $d\phi/dx$. Instead, due to the presence of the delta-function:

        $$\left.\frac{d\phi}{dx}\right|_{x_0+\epsilon} - \left.\frac{d\phi}{dx}\right|_{x_0-\epsilon}= -\frac{2mV_0}{\hbar^2}\phi(x_0).$$

    4. Find at which energy the boundary conditions at $x = x_0$ are satisfied. This is the energy of the bound state.
    5. Normalize the wave function.

Let us apply the LCAO model to find the molecular orbitals. As trial wave function for the molecular orbital, we consider a linear combination of the two atomic orbitals $|1\rangle$ and $|2\rangle$:

$$|\psi\rangle = \phi_1|1\rangle + \phi_2|2\rangle.$$

The orbitals $|1\rangle$ and $|2\rangle$ correspond to the wave functions of the electron when there is only a single delta peak present:

\begin{align}
H_1 |1\rangle &= \epsilon_0 |1\rangle,\\
H_2 |2\rangle &= \epsilon_0 |2\rangle.
\end{align}

We start by calculating the wavefunction of an electron bound to a single delta-function potential welll.
To do so, you first need to set up the Schrödinger equation for a single electron bound to a delta-function potential well.
You do not have to solve the Schrödinger equation twice—you can use the symmetry of the system to calculate the wavefunction of the other electron bound to the second delta-peak.

1. Find the atomic-orbital wave functions $|1\rangle$ and $|2\rangle$: $\psi_1(x)$ and $\psi_2(x)$. (see the hint)
Also find their energy $\epsilon_0$.
Remember that you need to normalize the wave functions.
2. Calculate the onsite energy and the hopping. Use this to construct the LCAO Hamiltonian. To simplify the calculations, assume that the orbitals are orthogonal.
3. Diagonalize the LCAO Hamiltonian to find the eigenenergies of the system.
It was previously mentioned that $V_0>0$.
Using this, determine which energy corresponds to the bonding energy.

### Exercise 4: Polarization of a hydrogen molecule in an electric field

Applying an electric field can induce an electric dipole moment in a molecule, yielding a 'polarized' molecule. The reason is that the electric field redistributes the charge over the molecule by exerting a force on its charged constituents. Here, we analyze the electric-field-induced dipole moment of a molecule using the LCAO model.

We consider a hydrogen molecule as a one-dimensional system with two identical nuclei at $x=-d/2$ and $x=+d/2$, so that the center of the molecule is at $x=0$.
Each atom contributes a single electron with charge $-e$. The LCAO Hamiltonian of the system is given by

$$
H_{\textrm{eff}} = \begin{pmatrix}
    E_0&&-t \\
    -t&&E_0
\end{pmatrix}.
$$

1. We apply an electric field $\mathcal{E}$ along the x-direction. Which term needs to be added to the Hamiltonian of each electron?

    ??? hint "The electric potential is given by"

        $$
        V_{\mathcal{E}}=- \int \mathcal{E} d x
        $$

2. Compute the LCAO Hamiltonian of the system in presence of the electric field. What are the new onsite energies of the two orbitals?
3. Diagonalize the modified LCAO Hamiltonian. Find the ground state wavefunction $\psi$. Note: for simplicity assume that the electric field is small, such that $\gamma \ll t$, with $\gamma = e d \mathcal{E}/2$.
4. Find the induced dipole moment $p$ of the molecule in the ground state. Check if your answer makes sense when you set the electric field to zero. Use that the ground state wave function you found in the previous subquestion is a linear superposition of two orthogonal orbitals centered at $-d/2$ and $+d/2$.

    ??? hint "Reminder: dipole moment"
        The dipole moment $p$ of a molecule with $n\leq 2$ electrons in its orbital ground state $|\psi\rangle$ is:

        $$
        p = n e \langle\psi|\hat{x}|\psi\rangle.
        $$

[^1]: See the book exercise 6.5 for relaxing the orthogonality assumption.

*[LCAO]: Linear combination of atomic orbitals
