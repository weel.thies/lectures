---
search:
  exclude: true
---
```python tags=["initialize"]
import matplotlib
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()

pi = np.pi
```

# Solutions for lecture 7 exercises

## Warm up exercises

1. The density of states is constant at small $\omega$ and diverges as $1/\sqrt{\omega_0-\omega}$ when $\omega\rightarrow \omega_0$.

2. The $\phi_n$ are the probability amplitudes of the atomic orbital basis states $|n\rangle$ in the LCAO wavefunction. The $x_n$ are the positions of atoms. The value of $k$ describes the spatial variation of the phase of the $\phi_n$. The probability of finding an electron varies spatially because the wavefunction is a sum of atomic orbitals that are localized on the atoms. More formally, it is given by $|\langle x|\Psi\rangle|^2 = \sum_n|\langle x|n\rangle|^2$, where the $\langle x|n\rangle = \psi_n(x)$ are the atomic orbitals.

3. An energy band has a width of $2\pi/a$ in k-space. If the distance between k-points is $2\pi/L$ and we have 2_s spin states, there are $2_sN$ states per band.

4. The group velocity is $v_g=d\omega/dk = \hbar^{-1}dE/dk$ with units of m/s. The effective mass is 
$$
m^* = \hbar^2\left(\frac{∂^2E}{∂k^2}\right)^{-1}.
$$

5. Differentiating the free-electron dispersion $\hbar^2k^2/2m_e$ twice yields $m^* = m_e$, where $m_e$ is the free electron mass. This is expected because the free electrons are not subject to a potential. The effective mass is given by the curvature of the dispersion. Therefore, if the dispersion has a constant curvature, the effective mass is the same for all $k-$values. This is the case for the purely parabolic dispersion $\epsilon(k)\propto^2 k^2$ of the free electron model

## Exercise 1: Lattice vibrations

1. The equation of motion is $m\ddot{u_n} = -\kappa(u_n-u_{n-1}) - \kappa(u_n-u_{n+1})$. To solve it, we use the Ansatz $u_n=e^{ikna-i\omega t}u_0$. Plugging this in, we obtain $m\omega^2 = 2\kappa(1-\cos(ka))$. This yields
$$
\omega = \omega_0 |\sin(ka/2)|,
$$
with $\omega_0=2\sqrt{\kappa/m}$, which is sketched in the lecture notes.

2. The group velocity is given by
$$
v_g(k)=\frac{\partial \omega(k)}{\partial k} = v_0\cos(\frac{ka}{2}) \text{sign}(k),
$$
where we defined $v_0 = a \sqrt{\frac{\kappa}{m}}$ and where $\text{sign}(k)$ represents the sign of $k$. (see plot at 4)

3. We observe that for each energy value, there are two crossings with the dispersion. To obtain the total density of states, we should sum over the partial density of states corresponding to the positive and negative $k$-values. To illustrate the procedure, we compute the partial density of states here explicitly
$$
g_{k>0}(\omega) = \frac{L}{2\pi} \left|\frac{1}{v_g}\right| = \frac{L}{2 \pi} \frac{1}{v_0\cos(ka/2)} = \frac{L}{2 \pi} \frac{1}{v_0\sqrt{1 - (\omega/\omega_0)^2}},
$$
where we used the dispersion relation to eliminate $k$ in favor of $\omega$. Since the partial density of states corresponding to negative k-values, $g_{k<0}$, is the same, the total density of states is
$$
g(\omega) = 2 g_{k>0}(\omega)
$$
The group velocity and density of states are plotted below:

```python
pyplot.subplot(1,2,1)
k = np.linspace(-pi+0.01, pi-0.01, 300)
pyplot.plot(k[0:149], np.sin(k[0:149])/(np.sqrt(1-np.cos(k[0:149]))),'b');
pyplot.plot(k[150:300], np.sin(k[150:300])/(np.sqrt(1-np.cos(k[150:300]))),'b');
pyplot.xlabel(r'$ka$'); pyplot.ylabel('$v(k)$');
pyplot.xticks([-pi, 0, pi], [r'$-\pi$', 0, r'$\pi$']);
pyplot.yticks([-np.sqrt(2), 0, np.sqrt(2)], [r'$-a\sqrt{\frac{\kappa}{m}}$', 0, r'$a\sqrt{\frac{\kappa}{m}}$']);
pyplot.tight_layout();

pyplot.subplot(1,2,2)
w = np.linspace(0, 0.95, 300);
g = 1/np.sqrt(1-w**2);
pyplot.plot(w, g, 'b');
pyplot.xlabel(r'$\omega$'); pyplot.ylabel('$g(w)$');
pyplot.xticks([0, 1], [0, r'$2\sqrt{\frac{k}{m}}$']);
pyplot.yticks([0.5, 1], [0, r'$\frac{L}{\pi a}\sqrt{\frac{m}{\kappa}}$']);
pyplot.tight_layout();
```

4.
For small $\omega$, we have a constant $g(\omega) \approx \frac{L}{\pi v_0}$ up to second order. The constant is as expected for a linear 1D dispersion.

For $\omega\rightarrow\omega_0$, the dispersion is approximately parabolic so we expect an inverse square root scaling. Defining the small number $\delta\omega = \omega-\omega_0$, we calculate $g(\delta\omega) \approx \frac{L}{\pi\sqrt{2}v_0}\frac{1}{\sqrt{-\delta\omega}}$. We indeed observe the expected scaling. Note that $\delta\omega<0$ and that $g(\omega>\omega_0)=0$.

5.Hint: To sketch the group velocity $v = \frac{d\omega}{dk}$, draw a coordinate system **under** or **above** the dispersion graph, with $k$ on the x-axis. To sketch $g(\omega)$, draw a coordinate system **next** to the dispersion, with *$g(\omega)$ on the x-axis*.

![](figures/dispersion_groupv_dos.svg)

## Exercise 2: Calculating the electronic band structure of a monatomic chain with both nearest- and next-nearest neighbour hoppings

1. The hopping is $-t = \langle n | H | n+1 \rangle = \langle n | \hat{V}_n | n+1 \rangle$, with $\hat{V}_n = V_0\delta(x-na)$. We thus find $-t \propto \int e^{-|x-na|/d}\delta(x-na) e^{-|x-(n+1)a|/d}dx = e^{-a/d}$ and similarly $-t' \propto e^{-2a/d}$

2. The Schrödinger equation is $H|\Psi\rangle = E|\Psi\rangle$. The wavefunction is $|\Psi\rangle = \sum_m \phi_m |m\rangle$. By calculating $\langle n |H|\Psi\rangle$, we find 
$$ 
E\phi_n = E_0\phi_n - t\phi_{n-1} - t\phi_{n+1} - t'\phi_{n-2} - t'\phi_{n+2}
$$

3. We solve the previous equation using the Ansatz $\phi_n=e^{ikna}\phi_0$. Doing so, we find the dispersion relation:
$$
E(k) = E_0 - 2t\cos(ka) - 2t'\cos(2ka)
$$

4. The effective mass is 
$$
m^* = \frac{\hbar^2}{2a^2}\frac{1}{t\cos(ka)+4t'\cos(2ka)}
$$
Plot for $t=t'$:

```python
k1 = np.linspace(-pi, -pi/2-0.01, 300);
k2 = np.linspace(-pi/2+0.01, pi/2-0.01, 300);
k3 = np.linspace(pi/2+0.01, pi, 300);

pyplot.plot(k1, 1/(5*np.cos(k1)),'b');
pyplot.plot(k2, 1/(5*np.cos(k2)),'b');
pyplot.plot(k3, 1/(5*np.cos(k3)),'b');
pyplot.xlabel(r'$k$'); pyplot.ylabel('$m_{eff}(k)$');
pyplot.xticks([-pi,0,pi],[r'$-\pi/a$',0,r'$\pi/a$']);
pyplot.yticks([],[]);
pyplot.tight_layout();
```

## Exercise 3: Vibrational heat capacity of a 1D monatomic chain

1. For the energy we have:
$$
E = \int \hbar \omega g(\omega) (n_{BE}(\hbar \omega) + \frac{1}{2})d\omega 
$$ 
with $g(\omega)$ the density of states calculated in exercise 1.3 and $n_{BE}(\hbar \omega) = \frac{1}{e^{\hbar\omega/k_BT}-1}$ the Bose-Einstein distribution.

2. For the heat capacity we have: 
$$
C = \frac{d E }{d T} = \int g(\omega) \hbar\omega \frac{d n_{BE}(\hbar \omega)}{d T}d\omega
$$

