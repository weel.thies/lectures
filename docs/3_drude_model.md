```python tags=["initialize"]
from matplotlib import pyplot
import matplotlib.animation as animation
from IPython.display import HTML
import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()
```

_(based on chapter 3 of the book)_  

!!! success "Expected prior knowledge"

    Before the start of this lecture, you should be able to:

    - Write down the equation describing the Lorentz force
    - Formulate Newton's equation of motion for a particle subject to a force, including a damping term
    - Discuss the concepts of voltage, current, resistivity, and conductivity
    - Describe how the number of phonons scales with temperature $T$ 

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Discuss the basics of Drude theory, which describes electron motion in metals.
    - Use Drude theory to analyze transport of electrons through conductors in electric and magnetic fields.
    - Describe concepts such as electron mobility and the Hall resistance.

## The starting point - Ohm's law

Ohm's law is an empirical observation of conductors, which states that voltage is proportional to current $V=IR$.
Since we are interested in *material* properties, we would like to rewrite this into a relation that does not depend on the conductor geometry.
We achieve this by expressing the terms in Ohm's law with their microscopic equivalents.
Consider a conducting wire with cross-sectional area $A$ and length $l$. 
Such a wire has resistance $R = \rho l / A$ where $\rho$ is the material-dependent resistivity. 
An applied voltage $V$ in the wire creates an electric field $E = V/l$.
The resulting current $I$ in the wire is described by the current density $j \equiv I / A$. 
Combining all these equations with Ohm's law, we get:

$$
V = I ρ \frac{l}{A} ⇒ E = ρ j,
$$

Now that we see that Ohm's law relates local quantities $E$ and $j$, let us try to understand how this relation may arise by considering motion of individual electrons conduct in metals.

## The Drude model - assumptions

We consider the motion of the individual electrons following Drude, who applied Boltzmann's kinetic theory of gases to electrons.
We start from the following reasonable assumptions about how electrons move:

- Electrons scatter randomly at uncorrelated times. The average time between scattering is $\tau$. Therefore, the probability of scattering in a time interval $dt$ is $dt / \tau$
- After each scattering event, the electron's momentum randomizes with a zero average $⟨\mathbf{p}⟩=0$
- Electrons are charged particles with charge $-e$, so that the Lorentz force $\mathbf{F}_L=-e\left(\mathbf{E}+\mathbf{v}×\mathbf{B}\right)$ acts on the electrons in between the scattering events

!!! warning "Electron charge"

    We are using a convention where $-e$ is the electron charge, so $e>0$.

The first assumption here is the least obvious: why does the time between scattering events not depend on e.g. electron velocity? There is no physical answer to this: the model is only an approximation.
The second assumption can be justified by symmetry: since we expect the electrons to scatter equally to all directions, their average velocity will be zero right after scattering.
Also note that for now we treat the electrons as classical particles, neglecting all quantum mechanical effects.
As we will see in the next lecture, quantum mechanical effects actually help to justify the first assumption.

Even under these simplistic assumptions, the trajectory of the electrons is hard to calculate.
Due to the random scattering, each trajectory is different, and this is how several example trajectories look:

```python
# Use colors from the default color cycle
default_colors = pyplot.rcParams['axes.prop_cycle'].by_key()['color']
blue, red = default_colors[0], default_colors[3]

walkers = 20 # number of particles
tau = 1 # relaxation time
gamma = .3 # dissipation strength
a = 1 # acceleration
dt = .1 # infinitesimal
T = 10 # simulation time

v = np.zeros((2, int(T // dt), walkers), dtype=float) #

# Select random time steps and scattering angles
np.random.seed(1)
scattering_events = np.random.binomial(1, dt/tau, size=v.shape[1:])
angles = np.random.uniform(high=2*np.pi, size=scattering_events.shape) * scattering_events
rotations = np.array(
    [[np.cos(angles), np.sin(angles)],
     [-np.sin(angles), np.cos(angles)]]
)

for step in range(1, v.shape[1]):
    v[:, step] = v[:, step-1]
    v[0, step] += a * dt
    v[:, step] = np.einsum(
        'ijk,jk->ik',
        rotations[:, :, step-1, :],
        v[:, step, :]
    ) * (1 - gamma * scattering_events[step-1])

r = np.cumsum(v * dt, axis=1)

scattering_positions = np.copy(r)
scattering_positions[:, ~scattering_events.astype(bool)] = np.nan

scatter_pts = scattering_positions

r_min = np.min(r.reshape(2, -1), axis=1) - 1
r_max = np.max(r.reshape(2, -1), axis=1) + 1

fig = pyplot.figure(figsize=(9, 6))
ax = fig.add_subplot(1, 1, 1)
ax.axis("off")
ax.set(xlim=(r_min[0], r_max[0]), ylim=(r_min[1], r_max[1]))

trajectories = ax.plot([],[], lw=1, color=blue, alpha=0.5)[0]
scatterers = ax.scatter([], [], s=20, c=red)

def frame(i):
    concatenated_lines = np.concatenate(
        (r[:, :i], np.nan * np.ones((2, 1, walkers))),
        axis=1
    ).transpose(0, 2, 1).reshape(2, -1)
    trajectories.set_data(*concatenated_lines)
    scatterers.set_offsets(scatter_pts[:, :i].reshape(2, -1).T)

anim = animation.FuncAnimation(fig, frame, interval=100)

pyplot.close()

HTML(anim.to_html5_video())
```

## The Drude equation of motion
Our goal is finding the *electric current density* $j$.
Each electron with charge $-e$ and velocity $\mathbf{v}$ carries current $-e\mathbf{v}$.
Therefore if the electron density is $n$, the *average* current they carry is $-ne⟨\mathbf{v}⟩$.
Our goal is thus to compute the *average* velocity.

The key idea is that although the motion of an individual electron is hard to calculate, the *average* motion of the electrons is much simpler.

For convenience from now on, we will omit the average brackets, and write $\mathbf{v}$ instead of $⟨\mathbf{v}⟩$.
This also applies to $F$.
We derive an equation of motion for the "average" electron in the following way:
Consider everything that happens in an (infinitesimal) time interval $dt$.
A fraction $dt/τ$ of the electrons scatters, and their average velocity becomes zero.

$$
m\mathbf{v}(t + dt) = 0
$$

The rest of the electrons $(1 - dt/τ)$ are accelerated by the Lorentz force $F$, so their velocity becomes

$$
m\mathbf{v}(t + dt) = m\mathbf{v}(t) + \mathbf{F}⋅dt.
$$

To find the average velocity, we take a weighted average of these two groups of particles:

\begin{align}
m\mathbf{v}(t+dt)  &= [m\mathbf{v}(t) + F dt]\left(1 - \frac{dt}{\tau}\right) + 0⋅\frac{dt}{\tau} \\
  &= [m\mathbf{v}(t) + \mathbf{F} dt] \left(1 - \frac{dt}{\tau}\right) \\
                  &= m\mathbf{v}(t) + dt [\mathbf{F} - m\frac{\mathbf{v(t)}}{\tau}] - \frac{\mathbf{F}}{\tau} dt^2
\end{align}

We now neglect the term proportional to $dt^2$ (it vanishes when $dt → 0$).
Finally, we recognize that $\left[\mathbf{v}(t+dt) - \mathbf{v}(t)\right]/dt = d\mathbf{v}(t)/dt$, which results in

$$
m\frac{d\mathbf{v}}{dt} = -m\frac{\mathbf{v}}{τ} + \mathbf{F}.
$$

Observe that the first term on the right-hand side has the same form as a drag force: it always decelerates the electrons.

This equation of motion describing the velocity of the average electron is our main result: now we only need to apply it.

## Key result of the Drude model: explaining resistivity
Let us first consider the case without magnetic fields, $\mathbf{B} = 0$, and a constant electric field $\mathbf{E}$.
After a long enough time, we expect the average electron velocity to become constant such that $d\mathbf{v}/dt = 0$. This yields the steady-state solution:

$$
\mathbf{v}=\frac{-eτ}{m}\mathbf{E}=-|μ|\mathbf{E}.
$$

Here, we defined the *mobility* $μ\equiv |e|τ/m$, which is the ratio between the electron *drift velocity* and the electric field.
Electron mobility describes the response of the electron to the electric field: for a given field, a higher $\mu$ means the electrons will (on average) move faster.
We substitute the steady-state velocity into the definition of current density:

$$
\mathbf{j}=-en\mathbf{v}=\frac{n e^2τ}{m}\mathbf{E}=\sigma\mathbf{E},\quad \sigma=\frac{ne^2τ}{m}=ne\mu,
$$

where $\sigma$ is the conductivity, so that $ρ=\frac{1}{\sigma}$. We have thus found a key result of the Drude model, namely how the resisitivity of a conductor depends on the electron density, charge, mass, and scattering time. Check if the scaling with these quantities makes sense! 

### On what do the electrons scatter?
We now look into the origins of electron scattering. 

The Drude model assumes that electrons scatter from all the atoms in a solid.
That, however, is not true (the next lecture explains why).
Instead, scattering is the result of deviations from a perfect crystal:

- Phonons: $τ_\mathrm{ph}(T)$ ($τ_\mathrm{ph}\rightarrow\infty$ as $T\rightarrow 0$)
- Impurities/vacancies or other crystalline defects: $τ_0$

Because the different scattering mechanisms are independent, the scattering rates $1/τ$ due to different mechanisms add up:

$$
\frac{1}{τ}=\frac{1}{τ_\mathrm{ph}(T)}+\frac{1}{τ_0}\ \Rightarrow\ ρ=\frac{1}{\sigma}=\frac{m}{ne^2}\left( \frac{1}{τ_\mathrm{ph}(T)}+\frac{1}{τ_0} \right)\equiv ρ_\mathrm{ph}(T)+ρ_0
$$

This approximate empirical observation is known as *Matthiessen's Rule* (1864).
It allows to separate the impurity scattering rate from the electron-phonon scattering rate by comparing temperature dependences of a clean and dirty metal.
The difference between the two resistances is due to the impurity scattering.


Let us apply the Drude model to compute a typical drift velocity of electrons in a metal. 
For example, consider a one meter long copper wire with a 1V voltage applied to it, $E = 1$ V/m.
Taking a scattering time of $τ∼25$ fs (valid in Cu at $T=300$ K), we obtain $v=\mu E=\frac{eτ}{m}E=2.5$ mm/s.

The electrons move unexpectedly slow—millimeters per second!

??? question "How come you do not have to wait for minutes until the light in your room turns on after you flick a switch?"

    How fast the light turns on is controlled by the speed at which the electric field develops inside the wire.
    Electric field propagates much faster than electrons—in vacuum it moves at the speed of light after all.

## Analyzing the 'Hall effect' using the Drude model
Conduction properties become much more interesting once we turn on both an electric field $\mathbf{E}$ and a magnetic field $\mathbf{B}$.
Let us consider a wire with current $\mathbf{j}$ flowing along the x-direction (notice how we are specifying the direction of the current and not of the electric field). Furthermore, we apply a magnetic field $\mathbf{B}$ in the z-direction, like shown in the figure:

![](figures/hall_effect.svg)


In the presence of both an electric and a magnetic field, the Drude equation of motion becomes

$$
m\frac{d\mathbf{v}}{dt} = -m\frac{\mathbf{v}}{τ} - e(\mathbf{E} + \mathbf{v}\times\mathbf{B})
$$

Once again, we consider the steady state $d\mathbf{v}/dt = 0$.
After substituting $\mathbf{v} = -\mathbf{j}/ne$, we arrive to

$$
\mathbf{E}=\frac{m}{ne^2τ}\mathbf{j} + \frac{1}{ne}\mathbf{j}\times\mathbf{B}.
$$

The first term is the same as before and describes the electric field parallel to the current. The second term describes an electric field **perpendicular** to the current.
In other words, if we send a current through a sample and apply a magnetic field, a voltage develops in the direction perpendicular to the current—this is called the *Hall effect*. The voltage is called *Hall voltage*, and the proportionality coefficient $B/ne$ the *Hall resistivity*.

Because of the Lorentz force, the electrons are deflected in a direction perpendicular to $\mathbf{B}$ and $\mathbf{j}$.
The deflection creates a charge imbalance, which in turn creates the electric field $\mathbf{E}_\mathrm{H}$ compensating the Lorentz force.

Because the above relation between the electric field and the current is linear, we can write it in matrix form

$$
E_a = ∑_b ρ_{ab} j_b,
$$

with $a, b ∈ \{x, y, z\}$, and $ρ$ the *resistivity matrix*.
Its diagonal elements are $ρ_{xx}=ρ_{yy}=ρ_{zz}=m/ne^2τ$—the same as without magnetic field.
The only nonzero off-diagonal elements when $\mathbf{B}$ points in the $z$-direction are

$$
ρ_{xy}=-ρ_{yx}=\frac{B}{ne}\equiv -R_H B,
$$

where $R_H=-1/ne$ is the *Hall coefficient*.
So by measuring the Hall voltage and knowing the electron charge, we can determine the density of free electrons in a material.

While most materials have $R_H < 0$, interestingly some materials are found to have $R_H > 0$. This would imply that the charge of the carriers is positive. We will see later in the course how to interpret this.

## Conclusions

1. Drude theory is a microscopic explanation of Ohm's law. 
2. It relates the electrical resistivity to the characteristic scattering time $\tau$ of the charge carriers via $\rho = \frac{m}{ne^2τ}$.
3. The Lorentz force leads to an electric field perpendicular to both the direction of electric current pushed through a material and the magnetic field. This is the Hall effect
4. By measuring the Hall voltage as a function of the magnetic field, we can determine the density of the charge carriers in a material. 

## Exercises

### Warm-up questions*

1. Write down the Drude equation of motion for the average velocity of electrons in a material subject to an electric field. Discuss the scaling of the drag force with $\tau$ and $m$.
2. From solving this equation in the steady state, we found the Drude conductivity $\sigma = \frac{ne^2\tau}{m}$. Discuss if its scaling with the electron density, charge, mass, and scattering time is reasonable.
3. Check that the units of mobility and the Hall coefficient are correct.  
   (Checking units is one of the fastest ways to see if an equation is incorrect!)
4. Explain why the scattering times due to different types of scattering events add up in a reciprocal way. 

### Exercise 1*: Extracting quantities from basic Hall measurements
Hall-voltage measurements in an applied magnetic field are a powerful tool for determining the free charge density in a material. Conversely, knowledge of the charge density enables measurements of magnetic fields, which is used in [Hall sensors](https://en.wikipedia.org/wiki/Hall_effect_sensor). Here we analyze how to extract such quantities from Hall measurements.

We consider a planar (two-dimensional) sample that sits in the $xy$ plane. The sample has width $W$ in the $y$-direction and length $L$ in the $x$-direction. We apply a current $I$ along the $x$-direction and a magnetic field $\bf B$ along the $z$-direction.

1.  Make a sketch of the system. Indicate where on the sample you can measure the Hall voltage $V_H$. Express the Hall resistance $R_{yx} = V_H/I$ as a function of $B = |\bf B|$. Does $R_{yx}$ depend on the geometry of the sample? Also express $R_{yx}$ in terms of the Hall coefficient $R_H$.

    ??? question "What is the relation between the electric field and the electric potential?"
        $V_b - V_a = -\int_{\Gamma} \mathbf{E} \cdot d\mathbf{\ell}$ if $\Gamma$ is a path from $a$ to $b$.

2. Assume we control the magnetic field $\mathbf{B}$. What quantity can we extract from a measurement of the Hall resistance (support your conclusion with the appropriate equation)? Would a large or a small magnetic field give a Hall voltage that is easier to measure? Would a large or small charge density make the Hall voltage easier to measure? Discuss if you find the final result surprising - we will come back to this later in the course when considering semiconductors.
3. Express the longitudinal resistance $R=V/I$, where $V$ is the voltage difference over the sample along the $x$ direction, in terms of the longitudinal resistivity $ρ_{xx}$. Suppose we extracted $n$ from a measurement of the Hall resistance, what quantity can we extract from a measurement of the longitudinal resistance? Does the result depend on the geometry of the sample?


### Exercise 2: The temperature dependence of resistance in the Drude model
The Drude model is based on the kinetic theory of gases. In this exercise, we try to explain the temperature dependence of the resistivity using this theory. It assumes that the characteristic scattering time $\tau$ is determined by the mean speed of the individual electrons (Note that this speed is not the same as the drift velocity $\bf v$ of the electrons(!), which, as argued in the lecture, should be zero in the absence of an electric field). Statistical physics tells us that the average kinetic energy of a particle in a gas at temperature $T$ equals $3k_BT/2$, yielding a root-mean-square (rms) speed equal to $v=\sqrt{\frac{3k_BT}{m}}$. We see that this speed depends on temperature. 

We consider copper, which has a density of 8960 kg/m$^3$, an atomic weight of 63.55 g/mol, and a room-temperature resistivity of $ρ=1.68\cdot 10^{-8}$ $\Omega$m. Each copper atom provides one free electron.

1. From these numbers, calculate the Drude scattering time $τ$ at room temperature. 
2. Assume that the electrons move with rms speed $v=\sqrt\frac{3k_BT}{m}$. Use this to estimate the electron mean free path $\lambda$, defined as the average distance an electron travels in between scattering events.
3. Now assume that the mean free path is independent of temperature. How would the electrical resistivity $ρ$ depend on temperature under this assumption? Discuss how it scales with temperature and sketch $ρ(T)$.
4. Compare your sketch of $ρ(T)$ with what you expect if electron scattering is caused by phonons of which the density $\propto T$.

### Exercise 3: The Hall conductivity matrix and the Hall coefficient
We apply a magnetic field $\bf B$ along the $z$-direction to a current-carrying 2D sample in the xy plane. As we have described in the lecture notes, in this situation, the electric field $\mathbf{E}$ is related to the current density $\mathbf{j}$ by the resistivity matrix:

$$
\mathbf{E} = \begin{pmatrix} ρ_{xx} & ρ_{xy} \\ ρ_{yx} & ρ_{yy} \end{pmatrix} \mathbf{j}
$$

1. Sketch the expressions for $ρ_{xx}$ and $ρ_{xy}$ derived in the lecture notes as a function of the magnetic field $\bf{B}$.
2. Invert the resistivity matrix to obtain the conductivity matrix,

    $$
    \begin{pmatrix} \sigma_{xx} & \sigma_{xy} \\ \sigma_{yx} & \sigma_{yy} \end{pmatrix}
    $$

    allowing you to express $\mathbf{j}$ as a function of $\mathbf{E}$.
    
3. Sketch $\sigma_{xx}$ and $\sigma_{xy}$ as a function of the magnetic field $\bf B$.
4. Give the definition of the Hall coefficient. What does the sign of the Hall coefficient indicate?

### Exercise 4: Positive and negative charge carriers
Measurements of the Hall voltage enable determining if the conduction of a sample is mediated by positive or negative charge carriers. In this question we analyze the Hall effect if we have both positive and negative charge carriers in a sample. (we will encounter positive charge carriers later in the course when discussing semiconductors). The negative charge carriers have density $n_e$, scattering time $\tau_e$, and charge $-e$. The positive charge carriers have density $n_h$, scattering time $\tau_h$, and charge $e$. The goal is to derive the Hall voltage and coefficient.

As before, we consider a 2D sample in the xy plane. We push a current through the sample by connecting electrodes and applying an electric field $E_x$ along the $+x$-direction. Furthermore, we apply a magnetic field $B_z$ along the $+z$-direction. The boundary conditions tell us that the net current in the $y$-direction should be zero. However, this does not mean the charge carriers do not move in the y-direction!

1. Express the current density $\bf J$ as a function of the drift velocities $\mathbf{v_e}$ and $\mathbf{v_h}$ of the charge carriers. 
2. Show that the drift velocities are given by $\mathbf{v_e} = -\mu_e(\mathbf{E} + \mathbf{v_e}\times\mathbf{B})$ and $\mathbf{v_h} = \mu_h(\mathbf{E} + \mathbf{v_h}\times\mathbf{B})$ 
3. Assume that $E_x \gg |v_{e,y}| B_z$ and $E_x \gg |v_{h,y}| B_z$ to show that $J_x = eE_x(n_e\mu_e + n_h\mu_h)$. Discuss the two terms of this last equation and why they should be added.
4. Under the same assumption, show that $J_y = eE_y(n_e\mu_e + n_h\mu_h) + eB_zE_x(n_e\mu_e^2-n_h\mu_h^2)$. What should $J_y$ be equal to?
5. Use the derived equations to show that the Hall electric field is
    $$ E_y = J_xB_z\frac{n_h\mu_h^2 - n_e\mu_e^2}{e(n_h\mu_h + n_e \mu_e)^2} $$
    Extract the Hall coefficient $R_H$ and discuss what determines its sign. 


