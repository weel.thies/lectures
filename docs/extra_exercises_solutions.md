
[comment]: <> (Your comment text
## extra exercises
### Exercise 1: the n-dimensional free electron model

1. Distance between nearest k-points is $\frac{2\pi}{L}$ and their density across n-dimensions is $(\frac{L}{2\pi})^n$.

2. 
    $$
    g_{1D}(k)\textrm{d} k = \left(\frac{L}{2\pi}\right) 2 \mathrm{d} k
    $$

    The factor 2 is due to positive and negative $k$-values having equal enery

    \begin{align}
    g_{2D}(k)\textrm{d} k &= \left(\frac{L}{2\pi}\right)^2 2\pi k \mathrm{d} k,\\
    g_{3D}(k)\textrm{d} k &= \left(\frac{L}{2\pi}\right)^3 4\pi k^2 \mathrm{d} k
    \end{align}

3. 

    \begin{align}
    g(k)\textrm{d} k &= \left(\frac{L}{2\pi}\right)^n S_{n-1}(k)\textrm{d} k \\
    &= \left(\frac{L}{2\pi}\right)^n \frac{2\pi^{\frac{n}{2}}k^{n-1}}{\Gamma(\frac{n}{2})}\textrm{d} k
    \end{align}

4. See hint of the question

5. 

    \begin{gather}
    g(\varepsilon)\textrm{d} \varepsilon = 2_s g(k)  \textrm{d} k\\
    g(\varepsilon)=2_sg(k(\varepsilon))\frac{\textrm{d} k}{\textrm{d} \varepsilon}\\
    g(\varepsilon) = \frac{2_s}{\Gamma(\frac{n}{2})}\left(\frac{L}{\hbar}\sqrt{\frac{m}{2\pi}}\right)^n (\varepsilon)^{\frac{n}{2}-1}
    \end{gather}

6. 

    $$
    N = \int_{0}^{\infty}g(\varepsilon)n_F(\beta(\varepsilon-\mu))\textrm{d} \varepsilon = \frac{2}{\Gamma(\frac{n}{2})}\left(\frac{L}{\hbar}\sqrt{\frac{m}{2\pi}}\right)^n\int_{0}^{\infty}\frac{(\varepsilon)^{\frac{n}{2}-1}}{e^{\frac{\varepsilon-\mu}{k_BT}}+1}\textrm{d} \varepsilon
    $$

    Total energy: $E = \int_{0}^{\infty} g(\varepsilon) n_{F}(\beta (\varepsilon - \mu)) \varepsilon \textrm{d} \varepsilon $

### Exercise 2: a hypothetical material

1.


$$
E = \int_{0}^{\infty}\varepsilon g(\varepsilon) n_{F}(\beta (\varepsilon - \mu)) \textrm{d} \varepsilon = 2.10^{10}eV^{-\frac{3}{2}}  \int_{0}^{\infty}\frac{\varepsilon^{\frac{3}{2}}}{e^\frac{\varepsilon-5.2}{k_BT}+1} \textrm{d} \varepsilon
$$

2.


$$
E = \frac{4}{5} (5.2)^{\frac{5}{2}} 10^{10} eV
$$

3.

\begin{align}
E(T)-E(T=0) &= \frac{\pi^2}{6}(k_B T)^2\frac{\partial}{\partial \varepsilon}\left(\varepsilon g(\varepsilon)\right)\bigg|_{\varepsilon=\varepsilon _F}\\
&\approx 8.356 10^8 eV
\end{align}

5.
$C_v = 1.6713.10^6 eV/K$

#### Questions 4, 6.

```
mu = 5.2
kB = 8.617343e-5
T = 1000 #kelvin

import numpy as np
from scipy import integrate

np.seterr(over='ignore')

# Fermi-Dirac distribution
def f(E, T):
    return 1 / (np.exp((E - mu)/(kB*T)) + 1)

# Density of states
def g(E):
    return 2e10 * np.sqrt(E)

#integration function
def integral(E, T):
    return f(E, T)*g(E)*E

## Solve integral numerically using scipy's integrate
dE = integrate.quad(integral, 0, 1e1, args=(T))[0] - 0.8e10 * 5.2**(5./2)

dT = 0.001
dEplus = integrate.quad(integral, 0, 1e1, args=(T+dT))[0] - 0.8e10 * 5.2**(5./2)
dEmin = integrate.quad(integral, 0, 1e1, args=(T-dT))[0] - 0.8e10 * 5.2**(5./2)

CV = (dEplus - dEmin) / (2*dT);

print(f'dE = {dE:.4e} eV')
print(f'Cv = {CV:.4e} eV/K')
```

```python
mu = 5.2
kB = 8.617343e-5
T = 1000 #kelvin

import numpy as np
from scipy import integrate

np.seterr(over='ignore')

# Fermi-Dirac distribution
def f(E, T):
    return 1 / (np.exp((E - mu)/(kB*T)) + 1)

# Density of states
def g(E):
    return 2e10 * np.sqrt(E)

#integration function
def integral(E, T):
    return f(E, T)*g(E)*E

## Solve integral numerically using scipy's integrate
dE = integrate.quad(integral, 0, 1e1, args=(T))[0] - 0.8e10 * 5.2**(5./2)

dT = 0.001
dEplus = integrate.quad(integral, 0, 1e1, args=(T+dT))[0] - 0.8e10 * 5.2**(5./2)
dEmin = integrate.quad(integral, 0, 1e1, args=(T-dT))[0] - 0.8e10 * 5.2**(5./2)

CV = (dEplus - dEmin) / (2*dT);

print(f'dE = {dE:.4e} eV')
print(f'Cv = {CV:.4e} eV/K')
```
Check the source code written in python for solving integral using midpoint rule.


*[DOS]: Density of states

)