---
search:
  exclude: true
---
# Solutions for Drude model exercises

### Exercise 1: Extracting quantities from basic Hall measurements
1. The Hall voltage is measured across the sample width. It is given by

    $$
    V_H = V(W)-V(0) = -\int_{0}^{W} E_ydy = -E_y W
    $$

    From the steady-state solution of the Drude equation of motion, we obtain $E_y = \rho_{yx}j_x = \frac{-B}{ne}j_x $. Using $j_x = I_x / W$ and $V_H=-E_y W$, we obtain $R_{yx} = V_H/I_x = \frac{B}{ne}$.
    so it does not depend on the sample geometry.


2. If the hall resistance and the magnetic field are known, we extract the charge density using $R_{yx} = \frac{B}{ne}$. Because $V_H = \frac{B}{ne}I_x$, a stronger field yields a larger Hall voltage, making it easier to measure. Likewise, a lower charge density yields a larger Hall voltage for a given bias current, making it easier to measure.

3. The longitudinal resistance is

    $$
    R_{xx} = \rho_{xx}\frac{L}{W}
    $$

    with $\rho_{xx} = \frac{m_e}{ne^2\tau}$ the longitudinal Drude resistivity. Therefore, knowing the electron density $n$, we can extract the scattering time ($\tau$). We observe that $R_{xx}$ depends on the sample geometry ($L$ and $W$), whereas the Hall resistance $R_{yx}$ does not.

<!---
#### Question 4.
See 3_drude_model.md

    $$
    m\left(\frac{d\bf v}{dt} + \frac{\bf v}{\tau}\right) = -e(\bf E + \bf v \times \bf B)
    $$
-->

### Exercise 2: Temperature dependence of resistance in the Drude model

1. We first find the electron density using $n_e =   \frac{ZnN_A}{W}$, where $Z=1$ is the number of free electrons per copper atom, *n* is the mass density of copper, $N_A$ is Avogadro's constant, and *W* is the atomic weight of copper. Then, we find the scattering time $\tau = 2.57 \cdot 10^{-14}$ s from the longitudinal Drude resistivity $\rho = \frac{m_e}{n_e e^2\tau}$.
2. Using $\lambda = \langle v \rangle\tau$, we get $\lambda =3$ nm.
3. The scattering time $\tau \propto \frac{1}{\sqrt{T}}$, such that we expect $\rho \propto \sqrt{T}$.
4. For most metals, the measured resistivity scales as $\rho \propto T$, in contrast with the $\rho \propto \sqrt{T}$ predicted by the Drude model. The linear scaling can be understood by assuming that the scattering is caused by phonons, as their number scales linearly with T at high temperature (recall that the high-temperature limit of the Bose-Einstein distribution functions is $n_B = \frac{kT}{\hbar\omega}$ leading to $\rho \propto T$). The inability to explain this linear dependence is a failure of the Drude model.

### Exercise 3: The Hall conductivity matrix and the Hall coefficient

1. $\rho_{xx}$ is independent of B and $\rho_{xy} \propto B$

2. The conductivities are 

    $$
    \sigma_{xx} = \frac{\rho_{xx}}{\rho_{xx}^2 + \rho_{xy}^2} = \frac{mne^2\tau}{m^2+e^2\tau^2 B^2}
    $$

    $$
    \sigma_{xy} = \frac{-\rho_{yx}}{\rho_{xx}^2 + \rho_{xy}^2} = \frac{Bne^3 \tau^2}{m^2+e^2 \tau^2 B^2}
    $$

3. These equations describe [Lorentzian](https://en.wikipedia.org/wiki/Spectral_line_shape#Lorentzian)-like functions.


4. The Hall coefficient is $R_H = -\frac{1}{ne}$. The sign of the Hall coefficient depends on sign of the charge carriers (analyzed further in the next exercise).


### Exercise 4. Positve and negative charge carriers

1. The current is the sum of the currents carried by the positive and negative charge carriers:

    $$ 
    \mathbf{J} = -n_e e \mathbf{ v_e} + n_h e \mathbf{ v_h}
    $$

2. Write down the Drude equation of motion for the positive and negative charge carriers separately and use $ \mu_e = \frac{e\tau_e}{m_e}$ and $ \mu_h = \frac{e\tau_h}{m_h}$

3. From the assumption, we can ignore the $(\mathbf{ v_i} \times \mathbf{B})_x$ terms. Then combining question 1 and 2 gives the result. 

4. The net current in the y-direction should be zero.

5. The Hall coefficient is

    $$
    R_H = \frac{n_h\mu_h^2 - n_e\mu_e^2}{e(n_h\mu_h + n_e \mu_e)^2}
    $$
    We observe that the sign of $R_H$ is determined by the density and the mobilty of the positive and negative charge carriers. Therefore, a measurement of the Hall voltage reveals the dominant charge carrier.
<!---

Solution extra exercise:


### Exercise: Motion of an electron in a magnetic and an electric field

#### Question 1.


$$
m\frac{d\bf v}{dt} = -e(\bf v \times \bf B)
$$

Magnetic field affects only the velocities along x and y, i.e., $v_x(t)$ and $v_y(t)$ as they are perpendicular to it. Therefore, the equations of motion for the electron are

$$
\frac{dv_x}{dt} = -\frac{ev_yB_z}{m}
$$

$$
\frac{dv_y}{dt} = \frac{ev_xB_z}{m}
$$

#### Question 2.
We can compute $v_x(t)$ and $v_y(t)$ by solving the differential equations in 1.

From

$$
v_x'' = -\frac{e^2B_z^2}{m^2}v_x
$$

and the initial conditions, we find $v_x(t) = v_0 \cos(\omega_c t)$ with $\omega_c=eB_z/m$. From this we can derive $v_y(t)=v_0\sin(\omega_c t)$.

We now calculate the particle position using $x(t)=x(0) + \int_0^t v_x(t')dt'$ (and similar for $y(t)$). From this we can find a relation between the $x$- and $y$-coordinates of the particle

$$
(x(t) - x_0)^2 + (y(t) - y_0)^2 = \frac{v_0^2}{\omega_c^2}.
$$

This equation describes a circular motion around the point $x_0=x(0), y_0=y(0)+v_0/\omega$, where the characteristic frequency $\omega_c$ is called the *cyclotron* frequency. Intuition: $\frac{mv^2}{r} = evB$ (centripetal force = Lorentz force due to magnetic field).

#### Question 3.
Due to the applied electric field $\bf E$ in the $x$-direction, the equations of motion acquire an extra term:

$$
m v_x' = -e(E_x + v_yB_z).
$$

Differentiating w.r.t. time leads to the same 2nd-order D.E. for $v_x$ as above. However, for $v_y$ we get

$$
v_y'' = -\omega_c^2(v_d+v_y),
$$

where we defined $v_d=\frac{E_x}{B_z}$. The general solutions are

$$
v_y(t) = c_1\sin(\omega_c t)+ c_2\cos(\omega_c t) -v_d \\
v_x(t) = c_3\sin(\omega_c t)+ c_4\cos(\omega_c t).
$$

Using the initial conditions $v_x(0)=v_0$ and $v_y(0)=0$ and the 1st order D.E. above, we can show

$$
v_y(t) = v_0\sin(\omega_c t)+ v_d\cos(\omega_c t) -v_d \\
v_x(t) = v_d\sin(\omega_c t)+ v_0\cos(\omega_c t).
$$

By integrating the expressions for the velocity we find:

$$
(x(t)-x_0)^2 + (y(t) - y_0 + v_d t))^2 = \frac{v_0^2}{\omega_c^2}.
$$

This represents a [cycloid](https://en.wikipedia.org/wiki/Cycloid#/media/File:Cycloid_f.gif): a circular motion around a point that moves in the $y$-direction with velocity $v_d=\frac{E_x}{B_z}$.

-->
