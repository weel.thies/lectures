---
search:
  exclude: true
---
# Solutions for LCAO model exercises

### Warm-up questions*

1. In general, for two wavefunctions, we do not have $\langle\psi_1|\psi_2\rangle=0$. Consider for instance two Gaussian orbitals that are identical except for being displaced with respect to each other.   
2. The sign of the hopping does not influence the spatial form of the eigenstates, nor does it change the eigenenergies
3. The size of $H$ increases with the number of atoms. 
4. The size of $H$ increases with the number of orbitals. 
5. The Hamiltonian is 

    $$
    H = \begin{pmatrix} E_0 & -t & 0 \\ -t & E_0 & -t \\ 0 & -t & E_0   \end{pmatrix}
    $$

    Including a hopping between atoms 1 and 3 changes the Hamiltonian to 

    $$
    H = \begin{pmatrix} E_0 & -t & -t \\ -t & E_0 & -t \\ -t & -t & E_0   \end{pmatrix}
    $$

6. In general, if we also consider the spin of the electron, the Hamiltonian for two atoms that each contribute one orbital is 4x4. However, if the energy of the electron does not depend on the spin, this 4x4 Hamiltonian is block diagonal with two identical  2x2 matrices describing the 2 electrons. The system is therefore completely described by one of these 2x2 matrices, the diagonalization of which will yield the wavefunction and energy that is valid for each spin state. 

### Exercise 1*: Shell-filling model of atoms

1. The order in which the orbitals of an atom are filled with electrons is described by the phenomenological Aufbau principle and Madelungs rule, described in the lecture notes.
2. The atomic number of Tungsten is 74. Using Madelungs rule, we get

    $$
    1s^22s^22p^63s^23p^64s^23d^{10}4p^65s^24d^{10}5p^66s^24f^{14}5d^4
    $$

3. Based on Madelung's rule, we would expect

    \begin{align}
    \textrm{Cu} &= [\textrm{Ar}]4s^23d^9\\
    \textrm{Pd} &= [\textrm{Kr}]5s^24d^8\\
    \textrm{Ag} &= [\textrm{Kr}]5s^24d^9\\
    \textrm{Au} &= [\textrm{Xe}]6s^24f^{14}5d^9
    \end{align}

    Deviations from this rule are predominantly caused by the interactions between the electrons, which render some orbitals more energetically favorable and others less so.

### Exercise 2* - a three-atom molecule

1. Same as warmup question 5.

    $$
    H =\begin{pmatrix} E_0 & -t & 0 \\ -t & E_0 & -t \\ 0 & -t & E_0   \end{pmatrix}
    $$

2. The components $\{\phi_i\}$ represent the probability amplitudes of finding the electron in atomic orbital $i$

3. Given the symmetry of the molecule, it seems reasonable that the probabilities of finding the electron on atoms 1 and 3 should be the same. I.e., $|\phi_1|^2=|\phi_3|^2$. We therefore guess the eigenvectors are of the form

    $$
    \mathbf{v} = \begin{pmatrix} 1 \\ a \\ 1 \end{pmatrix} \ \text{ and } \    \mathbf{u} = \begin{pmatrix} -1 \\ a \\ 1 \end{pmatrix}
    $$

4. Applying the Hamiltonian to $\mathbf{v}$ yields

    $$
    H\begin{pmatrix} 1 \\ a \\ 1 \end{pmatrix}  =  (E_0-at) \begin{pmatrix}  1 \\ \tfrac{E_0a-2t}{E_0-at} \\ 1 \end{pmatrix}
    $$

    from which we can see that $a=\tfrac{E_0a-2t}{E_0-at}$. Solving this yields two possible values $a=\pm\sqrt{2}$. The two associated eigenvalues are $E_0\pm at$.

    Similarly, applying the Hamiltonian to $\mathbf{u}$ yields

    $$
    H\begin{pmatrix} -1 \\ a \\ 1 \end{pmatrix}  =  \begin{pmatrix}  -E_0-at  \\ E_0a \\ E_0-at  \end{pmatrix}
    $$

    which can only be an eigenvector if $a=0$. We observe that the associated eigenenergy is $E_0$.

5. If we set the hopping between atoms 2 and 3 to zero, the system resembles two simpler systems: one isolated atom with eigenenergy $E_0$, and a two-atom molecule with eigenenergies $E\pm = E_0 \mp t$ and eigenvectors $\psi_\pm = [1 \quad \pm1 ]^\text{T}$.

### Exercise 3. Explicit calculation of the onsite and hopping energies from the wave functions of the atomic orbitals

1. The wave function of a bound state localized in a delta function well at $x_1$ is

    $$
    \psi_1(x) = \sqrt{\kappa}e^{-\kappa|x-x_1|},
    $$

    where $\kappa = \sqrt{\frac{-2mE}{\hbar^2}} = \frac{mV_0}{\hbar^2}$. This wave function is our atomic orbital. The energy of the atomic orbital is $\varepsilon_0 = -\frac{mV_0^2}{2\hbar^2}$

    $\psi_2(x)$ can be found by replacing $x_1$ by $x_2$

2. The matrix elements of the Hamiltonian in the basis of the atomic orbitals $|i \rangle$ and $|j\rangle$ are $\langle i | H | j\rangle $. Knowing the atomic orbitals, we can explicitly calculate these elements.

    The onsite energy is

    \begin{align}
        E_0 = \langle 1 | \hat{H} | 1\rangle & =  \langle 1 | \hat{V}_1 + \hat{V}_2 + \hat{K}| 1\rangle = E + \langle 1 | \hat{V}_2 | 1\rangle = E -V_0 \int \psi_1(x) \delta(x-x_2) \psi_1(x) dx \\
        =& \varepsilon_0 - V_0 \kappa e^{-2\kappa|x_2-x_1|}
    \end{align}  

    The hopping is

    \begin{align}
        -t =  \langle 1 | \hat{H} | 2\rangle & = \langle 1 | \hat{V}_1 + \hat{V}_2 + \hat{K}| 2\rangle =  \langle 1 | \hat{V}_1 | 2\rangle = -V_0 \int \psi_1(x) \delta(x-x_1) \psi_2(x) dx \\
        &=  -V_0 \kappa e^{-\kappa|x_2-x_1|}
    \end{align}  

    So we get

    $$
        H = -V_0\kappa\begin{pmatrix}
        1/2+\exp(-2\kappa|x_2-x_1|) &
        \exp(-\kappa|x_2-x_1|)\\
        \exp(-\kappa|x_2-x_1|) &
        1/2+\exp(-2\kappa|x_2-x_1|)
        \end{pmatrix}
    $$

3. The eigenvalues of a 2x2 matrix are  $\lambda_{\pm} = m\pm\sqrt{m^2-p}$, with $m$ the average of the diagonal entries and $p$ the determinant. Defining $\alpha = e^{-\kappa|x_2-x_1|}$, this yields

    $$
    \varepsilon_{\pm} = -V_0\kappa(\frac{1}{2}+\alpha^2 \pm \alpha)
    $$

    We see that, if we move the atoms further and further apart, $\alpha \rightarrow 0$ and the eigenenergies converge to those of the isolated atoms.

### Exercise 4: Polarization of a hydrogen molecule in an electric field

1. The electric field leads to a position-dependent potential energy of the electrons on the molecule, as given by  

    $$
        \hat{H}_{\mathcal{E}} = e\mathcal{E}\hat{x}.
    $$

    with matrix elements $\langle i | \hat{H}_{\mathcal{E}} | j\rangle $.

2. Adding $\hat{H}_\mathcal{E}$ to the Hamiltonian yields

    $$
    \hat{H} = \begin{pmatrix}
    E_0  & -t \\
    -t   & E_0 
    \end{pmatrix} +e\mathcal{E}\begin{pmatrix}
    \langle 1|\hat{x}|1\rangle & \langle 1|\hat{x}|2\rangle \\
    \langle 2|\hat{x}|1\rangle & \langle 2|\hat{x}|2\rangle
    \end{pmatrix} = \begin{pmatrix}
    E_0-\gamma & -t \\
    -t & E_0+\gamma
    \end{pmatrix},
    $$

    where we defined $\gamma = e d \mathcal{E}/2$ and used $\langle 1|\hat{x}|1\rangle = -\tfrac{d}{2}$

3. The eigenstates are given by:

    $$
    E_{\mp} = E_0\pm\sqrt{t^2+\gamma^2}
    $$

    Calling the elements of the eigenvector $\phi_1$ and $\phi_2$, we find

    $$
    \phi_1(E_0-\gamma)-\phi_2 t = \phi_1 E_\mp
    $$

    From this we find for the ground state 

    $$
    \phi_2 = -\frac{E_+- E_0 + \gamma}{t}\phi_1 = \frac{\sqrt{t^2+ \gamma^2} - \gamma }{t}\phi_1 \approx (1-\frac{\gamma}{t})\phi_1
    $$

    where the approximation follows from Taylor expanding the square root and the assumption that the electric field is small, such that $\gamma \ll t$. Then, using the normalization condition $\phi_1^2+\phi_2^2$=1, we find

    $$
    \phi_1\approx\frac{1}{\sqrt{2(1-\eta)}}
    $$

    where we defined $\eta=\gamma/t$.

4. We find the dipole moment using

    $$
    p=2e\langle\psi|\hat{x}|\psi\rangle = 2e\left(\phi_1^2\langle 1|\hat{x}| 1 \rangle + \phi_2^2\langle 2|\hat{x}| 2 \rangle \right) = ed(\phi_2^2-\phi_1^2) = -ed\frac{\eta}{1-\eta}
    $$

    Does the answer make sense for $\eta=0$?
